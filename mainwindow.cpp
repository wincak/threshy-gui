#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QComboBox>
#include <QSpinBox>
#include <QStatusBar>

const int BATTERY_Main = 1;
const int BATTERY_Auxiliary = 2;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->batterySelection->addItem(tr("Main"), BATTERY_Main);
    ui->batterySelection->addItem(tr("Auxiliary"), BATTERY_Auxiliary);

    client = new Client(this);

    if(client->connect()) {
        ui->statusBar->showMessage(tr("Ready"));
    } else {
        ui->statusBar->showMessage(client->errorString());
    }

    connect(ui->batterySelection, SIGNAL(currentIndexChanged(int)), this, SLOT(refreshBatteryThresholds(int)));
    connect(ui->dialogButtons, SIGNAL(clicked(QAbstractButton*)), this, SLOT(processButtonClick(QAbstractButton*)));
    connect(ui->fullCharge, SIGNAL(clicked()), this, SLOT(fullCharge()));

    // Initial battery selection
    refreshBatteryThresholds(ui->batterySelection->findData(BATTERY_Main));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::processButtonClick(QAbstractButton *button)
{
    QDialogButtonBox::ButtonRole role = ui->dialogButtons->buttonRole(button);

    if(role == QDialogButtonBox::ApplyRole) {
        setThresholds();
    } else if(role == QDialogButtonBox::AcceptRole) {
        setThresholds();
        QApplication::quit();
    } else {
        qDebug("%s Invalid button role %i", __PRETTY_FUNCTION__, role);
    }
}

void MainWindow::refreshBatteryThresholds(int batteryIndex)
{
    int battery = ui->batterySelection->itemData(batteryIndex).toInt();

    ui->startCharge->setValue(client->startThreshold(battery));
    ui->stopCharge->setValue(client->stopThreshold(battery));
}

void MainWindow::fullCharge()
{
    int battery = ui->batterySelection->itemData(ui->batterySelection->currentIndex()).toInt();

    client->fullCharge(battery);

    refreshBatteryThresholds(battery);
}

void MainWindow::setThresholds()
{
    int battery = ui->batterySelection->itemData(ui->batterySelection->currentIndex()).toInt();

    int startThreshold = ui->startCharge->value();
    client->setStartThreshold(battery, startThreshold);

    int stopThreshold = ui->stopCharge->value();
    client->setStopThreshold(battery, stopThreshold);
}
