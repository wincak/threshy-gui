#include "client.h"

#include <QtDBus>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusInterface>

Client::Client(QObject* parent)
    : QObject(parent)
{
    m_connectionValid = connect();
}

bool Client::connect()
{
    if (!QDBusConnection::systemBus().isConnected()) {
        m_connectionValid = false;
        m_errorString = tr("Failed to connect to DBus system bus");
        return (false);
    }

    m_pIface = new QDBusInterface("org.wincak.threshy", "/org/wincak/threshy", "org.wincak.threshy",
                                  QDBusConnection::systemBus(), this);

    if (!m_pIface->isValid()) {
        m_connectionValid = false;
        m_errorString = QDBusConnection::systemBus().lastError().message();
        return (false);
    }

    m_connectionValid = true;
    m_errorString.clear();

    return (true);
}

bool Client::connectionValid() const
{
    return (m_connectionValid);
}

QString Client::errorString() const
{
    return (m_errorString);
}

int Client::startThreshold(int battery)
{
    QDBusReply<int> reply;
    reply = m_pIface->call("getStartThreshold", battery);
    if(!reply.isValid()) {
        m_connectionValid = false;
        m_errorString = reply.error().message();
        qCritical("%s Failed to get start threshold. Error: %s", __PRETTY_FUNCTION__,
                  m_errorString.toLocal8Bit().data());
        return(-1);
    }

    return(reply.value());
}

int Client::stopThreshold(int battery)
{
    QDBusReply<int> reply;
    reply = m_pIface->call("getStopThreshold", battery);
    if(!reply.isValid()) {
        m_connectionValid = false;
        m_errorString = reply.error().message();
        qCritical("%s Failed to get stop threshold. Error: %s", __PRETTY_FUNCTION__,
                  m_errorString.toLocal8Bit().data());
        return(-1);
    }

    return(reply.value());
}

bool Client::inhibitCharge(int battery)
{
    // TODO
    Q_UNUSED(battery)
    return(false);
}

void Client::setStartThreshold(int battery, int threshold)
{
    QDBusReply<int> reply;
    reply = m_pIface->call("setStartThreshold", battery, threshold);
    if(!reply.isValid()) {
        m_connectionValid = false;
        m_errorString = reply.error().message();
        qCritical("%s Failed to set start threshold. Error: %s", __PRETTY_FUNCTION__,
                  m_errorString.toLocal8Bit().data());
    }
}

void Client::setStopThreshold(int battery, int threshold)
{
    QDBusReply<int> reply;
    reply = m_pIface->call("setStopThreshold", battery, threshold);
    if(!reply.isValid()) {
        m_connectionValid = false;
        m_errorString = reply.error().message();
        qCritical("%s Failed to set stop threshold. Error: %s", __PRETTY_FUNCTION__,
                  m_errorString.toLocal8Bit().data());
    }
}

void Client::fullCharge(int battery)
{
    QDBusReply<int> reply;
    reply = m_pIface->call("fullCharge", battery);
    if(!reply.isValid()) {
        m_connectionValid = false;
        m_errorString = reply.error().message();
        qCritical("%s Failed to set full charge mode. Error: %s", __PRETTY_FUNCTION__,
                  m_errorString.toLocal8Bit().data());
    }
}
