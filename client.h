#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>

class QDBusInterface;

class Client : public QObject
{
    Q_OBJECT

public:
    explicit Client(QObject *parent = nullptr);

    bool connect();

    bool connectionValid() const;
    QString errorString() const;

    int startThreshold(int battery);
    int stopThreshold(int battery);

    bool inhibitCharge(int battery);

signals:
    // TODO: should there be periodic monitoring? That would not make much sense. Or just true
    // asynchronous D-Bus calls?

public slots:
    void setStartThreshold(int battery, int threshold);
    void setStopThreshold(int battery, int threshold);

    void fullCharge(int battery);

private:
    QDBusInterface* m_pIface;
    bool m_connectionValid;
    QString m_errorString;
};

#endif // CLIENT_H
