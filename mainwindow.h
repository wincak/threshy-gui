#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QAbstractButton>

#include "client.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void processButtonClick(QAbstractButton* button);
    void refreshBatteryThresholds(int batteryIndex);
    void fullCharge();

private:
    void setThresholds();

private:
    Ui::MainWindow *ui;
    Client* client;
};

#endif // MAINWINDOW_H
